package com.extropicstudios.dyson.gui;

import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;

public interface Canvas {

	/**
	 * Write a string within the canvas using the given font.
	 * @param x
	 * @param y
	 * @param string
	 */
	public void drawString(float x, float y, String string, Font font);
	
	/**
	 * Draws an animation within the canvas.
	 * @param x
	 * @param y
	 * @param animation
	 */
	public void drawAnimation(float x, float y, String animation);
	
	/**
	 * Draws a UIElement inside the canvas.
	 * @param x
	 * @param y
	 * @param element
	 * @param g
	 */
	public void drawUIElement(float x, float y, UIElement element, Graphics g);
}
