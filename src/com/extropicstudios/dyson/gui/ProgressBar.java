package com.extropicstudios.dyson.gui;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.ShapeRenderer;

import com.extropicstudios.dyson.assets.ResourceContainer;

/**
 * A simple UI element that shows progress in bar form.
 * 
 * @author Joshua Stewart <chuzzum@gmail.com>
 */
public class ProgressBar extends WindowFrame {

	private Rectangle bar;

	private static final float BORDER = 1;
	private static float PADDING = 0;
	
	private int border;
	
	private long progress;
	private long max;
	
	public ProgressBar(ResourceContainer rc, float x, float y, float w, float h) {
	    super(rc, x,y,w,h);
	    setBorder(BORDER);
	    setPadding(PADDING);
		
		bar = new Rectangle(x+border, y+border, w-(border*2), h-(border*2)); 
	}

	public void setValues(long progress, long max) {
		this.progress = progress;
		this.max = max;
		resizeBar();
	}
	
	public void setProgress(long progress) {
		this.progress = progress;
		resizeBar();
	}
	
	private void resizeBar() {
		float width = (width() - (border*2)) * (progress / max);
		bar.setWidth(width);
	}

	@Override
	public void render(Graphics g, float xOffset, float yOffset) {
		if (isHidden())
			return;
		
		super.render(g, xOffset, yOffset);
		Rectangle renderBar = new Rectangle(bar.getX() + xOffset, bar.getY() + yOffset, bar.getWidth(), bar.getHeight());
		ShapeRenderer.fill(renderBar);
	}

}
