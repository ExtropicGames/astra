package com.extropicstudios.dyson.gui;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.RoundedRectangle;
import org.newdawn.slick.geom.ShapeRenderer;

import com.extropicstudios.dyson.assets.ResourceContainer;

/**
 * A {@link WindowFrame} that can contain any number of {@link TabPanel}s
 * and allows them to be switched between.
 * 
 * @author Joshua Stewart <chuzzum@gmail.com>
 */
public class TabbedWindow extends WindowFrame {
	
    private static final float BORDER = 10;
    private static final float PADDING = 0;
    private static final boolean ROUNDED = true;
    
    private Runnable exitAction = new Runnable() { public void run() {}};
    
	private int activeTab;
	
	private List<Tab> tabs;
	
	private GradientFill selectedFill;
	private GradientFill unselectedFill;
	
	public TabbedWindow(ResourceContainer rc, float x, float y, float w, float h) {
	    super(rc, x,y,w,h);
	    
		setBorder(BORDER);
		setRounded(ROUNDED);
		setPadding(PADDING);
		
		tabs = new ArrayList<Tab>();
	}
	
	public TabbedWindow setExitAction(Runnable exitAction) {
		this.exitAction = exitAction;
		return this;
	}
	
	public void addTab(String title, TabPanel panel) {
		tabs.add(new Tab(rc, title, panel));
		initialize();
	}
	
	private void initialize() {
		float tabWidth = canvasW() / tabs.size();
		float tabHeight = rc.getDefaultFont().getLineHeight() + (padding()*2);
		unselectedFill = new GradientFill(0f, 0f, Color.blue, 0f, tabHeight, Color.black);
		unselectedFill.setLocal(true);
		selectedFill = new GradientFill(0f, 0f, Color.green, 0f, tabHeight, Color.black);
		selectedFill.setLocal(true);
		
		int i = 0;
		for (Tab t : tabs) {
			if (i == activeTab)
				t.isActive = true;
			else
				t.isActive = false;
			t.setBounds(canvasX() + (i*tabWidth), canvasY(), tabWidth, tabHeight);
			i++;
			t.panel.setBounds(canvasX(), canvasY() + tabHeight, canvasW(), canvasH() - tabHeight);
		}
	}
	
	@Override
	public void processInput(Input input) {
		// active tab does stuff here
		if (tabs.get(activeTab).hasCursor()) {
			tabs.get(activeTab).processInput(input);
		}
		// i do stuff here
		else if (input.isKeyPressed(Input.KEY_A)) {
			if (activeTab > 0) {
				tabs.get(activeTab).isActive = false;
				activeTab--;
				tabs.get(activeTab).isActive = true;
			}
		} else if (input.isKeyPressed(Input.KEY_D)) {
			if (activeTab < tabs.size() - 1) {
				tabs.get(activeTab).isActive = false;
				activeTab++;
				tabs.get(activeTab).isActive = true;
			}
		} else if (input.isKeyPressed(Input.KEY_S)) {
			tabs.get(activeTab).giveCursor();
		} else if (input.isKeyPressed(Input.KEY_ESCAPE)) {
			exitAction.run();
		}
	}
	
	@Override
	public void render(Graphics g, float xOffset, float yOffset) {
		if (isHidden())
			return;
		
		super.render(g, xOffset, yOffset);
		
		for (Tab t : tabs) {
			t.render(g, xOffset, yOffset);
		}
	}
	
	private class Tab extends UIElement {
		private RoundedRectangle frame;
		private TabPanel panel;
		private String title;
		
		private boolean isActive;
		
		public Tab(ResourceContainer rc, String title, TabPanel panel) {
			super(rc);
			this.panel = panel;
			this.title = title;
		}
		public void setBounds(float x, float y, float w, float h) {
			frame = new RoundedRectangle(x,y,w,h, 10, 10, RoundedRectangle.TOP_LEFT & RoundedRectangle.TOP_RIGHT);
		}
		
		@Override
		public void render(Graphics g, float xOffset, float yOffset) {
			if (this.isHidden())
				return;
			
			Font uniFont = rc.getDefaultFont();

			if (isActive) {
				ShapeRenderer.fill(frame, selectedFill);
			} else {
				ShapeRenderer.fill(frame, unselectedFill);
			}
			uniFont.drawString(frame.getX() + padding(), frame.getY() + padding(), title);

			if (isActive)
				panel.render(g, xOffset, yOffset);
		}
		
		@Override
		public void processInput(Input input) {
			panel.processInput(input);
		}
		
		public boolean hasCursor() {return panel.hasCursor();}
		public void giveCursor() {panel.giveCursor();}
	}

}
