package com.extropicstudios.dyson.gui;

import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.dyson.assets.ResourceAccessor;
import com.extropicstudios.dyson.assets.ResourceContainer;

/**
 * The base class for all GUI elements. Allows the element to be rendered,
 * moved around, and to process input.
 * 
 * @author Joshua Stewart <chuzzum@gmail.com>
 */
public abstract class UIElement extends ResourceAccessor implements Canvas {
    
    private boolean isHidden;
    private float x;
    private float y;
    
    public UIElement(ResourceContainer rc) {
		super(rc);
	}
    
    public float x() { return x; }
    public float y() { return y; }
    public void setX(float x) { this.x = x; }
    public void setY(float y) { this.y = y; }
	public void show() { isHidden = false; }
	public void hide() { isHidden = true; }
	
	/**
	 * If true, the window should never render itself.
	 * @return
	 */
	public boolean isHidden() { return isHidden; }
	
	public void processInput(Input input) {
	}
	
	public abstract void render(Graphics g, float xOffset, float yOffset);
	
	@Override
	public void drawString(float xOffset, float yOffset, String string, Font font) {
	    font.drawString(x + xOffset, y + yOffset, string);
	}
	
	@Override
	public void drawAnimation(float xOffset, float yOffset, String animation) {
	    rc.getAnimation(animation).draw(x + xOffset, y + yOffset);
	}
	
	@Override
	public void drawUIElement(float xOffset, float yOffset, UIElement element, Graphics g) {
	    element.render(g, x + xOffset, y + yOffset);
	}
}