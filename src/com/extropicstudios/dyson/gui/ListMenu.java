package com.extropicstudios.dyson.gui;

import java.util.List;

import org.newdawn.slick.Graphics;

import com.extropicstudios.dyson.assets.ResourceContainer;

/**
 * An implementation of {@link ChoiceMenu} that presents the choices in a list
 * with a selector that can be moved with the arrow keys.
 * @author josh
 *
 */
public class ListMenu extends ChoiceMenu {
    
    private static final float LIST_BORDER = 10;
    private static final float LIST_PADDING = 2;
    private static final float LINE_SPACING = 2;
    private static final float ICON_SPACING = 4;
    
    private final String cursorIcon;
		
	public ListMenu(ResourceContainer rc, List<Option> options, String cursorIcon) {
	    super(rc, 0,0,0,0);
	    setBorder(LIST_BORDER);
	    setPadding(LIST_PADDING);
	    
	    this.cursorIcon = cursorIcon;
		this.options = options;
		initialize();
	}
	
	private void initialize() {
		float width = 0;
		// iterate through the options and set the width of the window equal to the width of the longest option 
		for (Option op : options) {
			float length = rc.getDefaultFont().getWidth(op.name);
			if (length > width)
				width = length;
		}
		width += (LIST_BORDER*2) + rc.getAnimation(cursorIcon).getWidth() + ICON_SPACING + (LIST_PADDING*2);
		
		float height = (rc.getDefaultFont().getLineHeight() * (LINE_SPACING*2)) * options.size();
		height += (LIST_BORDER*2) + (LIST_PADDING*2);
		
		setBounds(10, 10, width, height);
		setRounded(true);
		setBorder(LIST_BORDER);
	}

	@Override
	public void render(Graphics g, float xOffset, float yOffset) {
		if (isHidden())
			return;
		
		super.render(g, xOffset, yOffset);

		//Animation cursorAnim = rm.getAnimation(cursor);
		
		float cursorHeight = rc.getAnimation(cursorIcon).getHeight();
		float lineHeight = rc.getDefaultFont().getLineHeight();
		
		boolean isCursorBiggerThanText = cursorHeight >= lineHeight;
		
		float textOffset = 0;
		if (isCursorBiggerThanText)
			textOffset += (cursorHeight - lineHeight) / 2;
		for (int i = 0; i < options.size(); i++) {	
			if (i == currentSelection) {
				float cursorOffset;
				if (!isCursorBiggerThanText)
					cursorOffset = textOffset + ((lineHeight - cursorHeight) / 2);
				else
					cursorOffset = textOffset;
				
				this.drawAnimation(0, cursorOffset, cursorIcon);
			}
			drawString(rc.getAnimation(cursorIcon).getWidth() + ICON_SPACING, textOffset, options.get(i).name, rc.getDefaultFont());
			textOffset += LINE_SPACING + lineHeight;
			if (isCursorBiggerThanText)
				textOffset += (cursorHeight - lineHeight) / 2;
		}
	}

}
