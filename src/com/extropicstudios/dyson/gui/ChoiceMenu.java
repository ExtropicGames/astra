package com.extropicstudios.dyson.gui;

import java.util.List;

import org.newdawn.slick.Input;

import com.extropicstudios.dyson.assets.ResourceContainer;

/**
 * An abstract base for any menu that involves choosing an item
 * from a list of choices.
 * @author joshua
 */
public abstract class ChoiceMenu extends WindowFrame {
	
	public ChoiceMenu(ResourceContainer rc, float x, float y, float w, float h) {
        super(rc, x, y, w, h);
    }
	
	protected List<Option> options;
	
	protected class Option {
	    public Option(String icon, String name, Runnable action) {
            this.name = name;
            this.action = action;
            this.icon = icon;
        }
        public Runnable action;
	    public String name;
	    public String icon;
	}
	
	protected int currentSelection;
	
	@Override
	public void processInput(Input input) {
		if (input.isKeyPressed(Input.KEY_W)) {
			if (currentSelection > 0)
				currentSelection--;
		} else if (input.isKeyPressed(Input.KEY_S)) {
			if (currentSelection < options.size() - 1)
				currentSelection++;
		} else if (input.isKeyPressed(Input.KEY_SPACE)) {
			options.get(currentSelection).action.run();
		}
	}

}
