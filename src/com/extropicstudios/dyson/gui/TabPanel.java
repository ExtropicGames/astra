package com.extropicstudios.dyson.gui;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

/**
 * An interface that defines UI elements that can be placed in a {@link TabbedWindow}.
 * 
 * @author Joshua Stewart <chuzzum@gmail.com>
 */
public interface TabPanel {

	public boolean hasCursor();

	public void giveCursor();
	
	public void setBounds(float x, float y, float w, float h);
	
	public void processInput(Input input);
    
    public void render(Graphics g, float xOffset, float yOffset);
}
