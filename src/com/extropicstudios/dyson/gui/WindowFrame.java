package com.extropicstudios.dyson.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.RoundedRectangle;
import org.newdawn.slick.geom.ShapeRenderer;

import com.extropicstudios.dyson.assets.ResourceContainer;

/**
 * An extension to {@link UIElement} that adds a window frame,
 * border padding, fonts, and corner rounding, as well as basic
 * rendering functionality.
 * 
 * @author Joshua Stewart <chuzzum@gmail.com>
 */
public class WindowFrame extends UIElement {
	protected static String menuFont;
	private static UnicodeFont uniFont;
	
	private float w;
	private float h;
	
	/** The thickness of the window frame. */
	private float border;
	private float padding;
	
	//protected Rectangle frame;
	protected GradientFill frameFill;
	//protected Rectangle interior;
	protected GradientFill interiorFill; 
	
	private boolean rounded;
	private int roundedness; 
	
	//private int GradientType;
	
	public WindowFrame(ResourceContainer rc, float x, float y, float w, float h) {
		super(rc);
		roundedness = 10;

		frameFill = new GradientFill(0f, 0f, Color.darkGray, w, h, Color.lightGray);
        frameFill.setLocal(true);
        interiorFill = new GradientFill(0f, 0f, Color.blue, 0f, interiorH()*2, Color.black);
        interiorFill.setLocal(true);
		
		setX(x);
		setY(y);
		setW(w);
		setH(h);
	}
	
	public void setBounds(float x, float y, float w, float h) {
	    setX(x);
	    setY(y);
	    setW(w);
	    setH(h);
		frameFill.setEnd(w, h);
		interiorFill.setEnd(0f, interiorH()*2);
	}
	
	public void setWindowColors(Color c1, Color c2) {
		interiorFill.setStartColor(c1);
		interiorFill.setEndColor(c2);
	}
	
	public void setBorderColors(Color c1, Color c2) {
		frameFill.setStartColor(c1);
		frameFill.setEndColor(c2);
	}
	
	public void setW(float w) {
	    this.w = w;
		frameFill.setEnd(w, h);
	}
	
	public void setH(float h) {
	    this.h = h;
		frameFill.setEnd(w, h);
		interiorFill.setEnd(0f, interiorH()*2);
	}
	
    public void setCanvasH(float h) { setH(h + (border*2) + (padding*2)); }
    public void setCanvasW(float w) { setW(w + (border*2) + (padding*2)); }
	public void setRounded(boolean rounded) { this.rounded = rounded; }
	public void setBorder(float border) { this.border = border; }
	public void setPadding(float padding) { this.padding = padding; }
	
	@Override
	public void render(Graphics g, float xOffset, float yOffset) {
		if (isHidden())
			return;
		
		Rectangle renderFrame;
		Rectangle renderInterior;
		if (rounded == false) {
		    renderFrame = new Rectangle(x() + xOffset, y() + yOffset, w, h);
		    renderInterior = new Rectangle(interiorX() + xOffset, interiorY() + yOffset, interiorW(), interiorH());
		}
		else {
		    renderFrame = new RoundedRectangle(x() + xOffset, y() + yOffset, w, h, roundedness);
		    renderInterior = new RoundedRectangle(interiorX() + xOffset, interiorY() + yOffset, interiorW(), interiorH(), roundedness);
		}
		
		ShapeRenderer.fill(renderFrame, frameFill);
		ShapeRenderer.fill(renderInterior, interiorFill);
	}
	
//	public static void setMenuFont(String font) {
//		menuFont = font;
//		uniFont = ResourceManager.getInstance().getFont(font);
//	}

	@Override
    public void drawString(float xOffset, float yOffset, String string, Font font) {
       super.drawString(padding + border + xOffset, padding + border + yOffset, string, font);
    }
	
	@Override
	public void drawAnimation(float xOffset, float yOffset, String animation) {
	    super.drawAnimation(padding + border + xOffset, padding + border + yOffset, animation);
	}
	
	@Override
	public void drawUIElement(float xOffset, float yOffset, UIElement element, Graphics g) {
	    super.drawUIElement(padding + border + xOffset, padding + border + yOffset, element, g);
	}
	
	public boolean getRounded() { return rounded; }
	public float width() {return w;}
	public float height() {return h;}
	private float interiorX() {return x()+border;}
	private float interiorY() {return y()+border;}
	private float interiorW() {return w - (border*2);}
	private float interiorH() {return h - (border*2);}
	public float canvasX() {return x()+border+padding;}
	public float canvasY() {return y()+border+padding;}
	public float canvasW() {return w - ((border*2) + (padding*2));}
	public float canvasH() {return h - ((border*2) + (padding*2));}
	public float border() {return border;}
	public float padding() {return padding;}

	public int getFontHeight() {return uniFont.getLineHeight();}
}
