package com.extropicstudios.dyson.gui;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import com.extropicstudios.dyson.assets.ResourceContainer;

// TODO: Future improvements:
// Make border color customizable
// Have UI constants loaded from XML
// When menu is long, it sometimes goes down below the point it started from.
// Menu should intelligently reposition itself around a selected region.

/**
 * A skin for a {@link ChoiceMenu} that has icons for each selection
 * and highlights the currently chosen selection.
 * 
 * @author Joshua Stewart <chuzzum@gmail.com>
 */
public class PopUpMenu extends ChoiceMenu {
    
    private static final float POPUP_BORDER = 2;
    private static final float POPUP_PADDING = 0;
    private static final boolean POPUP_ROUNDED = true;
    
    private static final float SELECTION_BORDER = 2;
    private static final float SELECTION_PADDING = 4;
    private static final boolean SELECTION_ROUNDED = true;
    
    private static final Color POPUP_COLOR_1 = Color.blue;
    private static final Color POPUP_COLOR_2 = Color.black;
    //private static final Color POPUP_COLOR_1 = new Color(32,32,32);
    //private static final Color POPUP_COLOR_2 = new Color(255, 0, 0);
    //private static final Color POPUP_SELECTION_COLOR_1 = new Color(128,128,128);
    private static final Color POPUP_SELECTION_COLOR_1 = Color.green;
    private static final Color POPUP_SELECTION_COLOR_2 = Color.black;
    
    private static final float ICON_SPACING = 8;
    
	private Vector2f parentSize;
	private Rectangle startingRegion;
	
	private WindowFrame selectionFrame;
	
	/**
	 * Creates a new PopUpMenu extending out from the point
	 * given. The PopUpMenu will move itself around in order
	 * to fit on the screen.
	 */
	public PopUpMenu(ResourceContainer rc, float x, float y, int parentHeight, int parentWidth) {
	    super(rc, x,y,0,0);
	    setBorder(POPUP_BORDER);
	    setPadding(POPUP_PADDING);
	    setRounded(POPUP_ROUNDED);
	    setWindowColors(POPUP_COLOR_1, POPUP_COLOR_2);
	    
	    startingRegion = new Rectangle(x,y,0,0);
	    
		options = new ArrayList<Option>();

		selectionFrame = new WindowFrame(rc, 0,0,0,0);
		selectionFrame.setWindowColors(POPUP_SELECTION_COLOR_1, POPUP_SELECTION_COLOR_2);
		selectionFrame.setBorder(SELECTION_BORDER);
		selectionFrame.setPadding(SELECTION_PADDING);
		selectionFrame.setRounded(SELECTION_ROUNDED);
		
		parentSize = new Vector2f(parentWidth, parentHeight);
		
		initialize();
	}
	
    private void initialize() {
        float fontHeight = rc.getDefaultFont().getLineHeight();
        selectionFrame.setCanvasH(fontHeight);
        setCanvasH(selectionFrame.height() * options.size());

        float maxWidth = 0;
        for (int i = 0; i < options.size(); i++) {
            float lineWidth = rc.getAnimation(options.get(i).icon).getWidth() + ICON_SPACING;
            lineWidth += rc.getDefaultFont().getWidth(options.get(i).name);
            if (lineWidth > maxWidth)
                maxWidth = lineWidth;
        }
        selectionFrame.setCanvasW(maxWidth);
        setCanvasW(selectionFrame.width());

        // Move the window if necessary so it doesn't go out of the parent
        // container. If the box is too close to the left edge, move it back
        // from the edge.
        if (startingRegion.getX() + width() > parentSize.x) {
            setX(startingRegion.getX() - width());
        } else {
            setX(startingRegion.getX());
        }

        // We normally want the window to go up from the origin point, so set it
        // to do so if we can. If we are too close to the top to do so, leave it
        // where it is.
        if (startingRegion.getY() - height() > 0) {
            setY(startingRegion.getY() - height());
        } else {
            setY(startingRegion.getY());
        }

        currentSelection = 0;
    }
	
	
    public void addOption(String icon, String name, Runnable action) {
        options.add(new Option(icon, name, action));
        initialize();
    }
	
    @Override
	public void render(Graphics g, float xOffset, float yOffset) {
        if (isHidden())
            return;
        
		super.render(g, xOffset, yOffset);

		// Each line is selectionFrame.height() pixels in height.
		
		for (int i = 0; i < options.size(); i++) {
		    float iconHeight = rc.getAnimation(options.get(i).icon).getHeight();
		    float iconWidth = rc.getAnimation(options.get(i).icon).getWidth();
			// center the icon with the text
		    int iconOffset = (int) (selectionFrame.canvasH() - iconHeight) / 2;
		    selectionFrame.setY(selectionFrame.height() * i);
			if (i == currentSelection) {
				drawUIElement(xOffset, yOffset, selectionFrame, g);
			}
			selectionFrame.drawAnimation(canvasX() + xOffset, canvasY() + iconOffset + yOffset, options.get(i).icon);
			selectionFrame.drawString(canvasX() + iconWidth + ICON_SPACING + xOffset, canvasY() + yOffset, options.get(i).name, rc.getDefaultFont());
		}
	}
}
