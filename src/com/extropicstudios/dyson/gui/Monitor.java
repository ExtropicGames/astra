package com.extropicstudios.dyson.gui;

import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.dyson.FormatString;
import com.extropicstudios.dyson.assets.ResourceContainer;

/**
 * A class that overlays text on the screen and can be set to update its
 * value every frame. It is intended to be used for monitoring changes
 * in a variable such as the player's health.
 * 
 * @author Joshua Stewart <chuzzum@gmail.com>
 */
public class Monitor extends UIElement {
	
	public enum Type {NORMAL, RISING, MARQUEE_LEFT, MARQUEE_RIGHT, MARQUEE_UP, MARQUEE_DOWN}

	private Runnable exitAction = new Runnable() {public void run() {}};
	
	private Type type;
	
	private float x;
	private float y;
	
	private float parentWidth;
	private float parentHeight;
	
	private FormatString string;
	
	public Monitor(ResourceContainer rc, String label, Object object) {
		this(rc, label, object, 0, 0, Type.NORMAL);
	}
	
	public Monitor(ResourceContainer rc, String label, Object object, float parentWidth, float parentHeight, Type type) {
		super(rc);
		
	    this.parentWidth = parentWidth;
	    this.parentHeight = parentHeight;
	    
		List<Object> list = new LinkedList<Object>();
		list.add(object);
		// TODO: This code seems suspect. Double-check it.
		if (label.indexOf("%v") == -1)
			string = new FormatString(label + "%v", list);
		else
			string = new FormatString(label, list);
		
		this.type = type;
	}
	
	// ===================
	//  Set configuration
	// ===================
	
	public Monitor setExitAction(Runnable exitAction) {
		this.exitAction = exitAction;
		return this;
	}
	
	public Monitor setParentWidth(float parentWidth) {
		this.parentWidth = parentWidth;
		return this;
	}
	
	public Monitor setParentHeight(float parentHeight) {
		this.parentHeight = parentHeight;
		return this;
	}
	
	public Monitor setType(Type type) {
		this.type = type;
		return this;
	}
	
	public void move(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public void processInput(Input input) {
	    if (y < 0 && type == Type.RISING) {
	        exitAction.run();
	    }
	}
	
	public void update() {
		if (type == Type.RISING) {
			y--;
		} else if (type == Type.MARQUEE_LEFT) {
			x--;
			if (x < 0) {
				x = parentWidth;
			}
		} else if (type == Type.MARQUEE_RIGHT) {
			x++;
			if (x > parentWidth) {
				x = 0;
			}
		} else if (type == Type.MARQUEE_UP) {
			y--;
			if (y < 0) {
				y = parentHeight;
			}
		} else if (type == Type.MARQUEE_DOWN) {
			y++;
			if (y > parentHeight) {
				y = 0;
			}
		}
	}

	@Override
	public void render(Graphics g, float xOffset, float yOffset) {
		if (isHidden())
			return;
		
		drawString(x + xOffset, y + yOffset, string.toString(), rc.getDefaultFont());
	}
}
