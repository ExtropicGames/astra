package com.extropicstudios.dyson.assets;

public abstract class ResourceAccessor {

    protected final ResourceContainer rc;
    
    public ResourceAccessor(ResourceContainer rc) {
    	this.rc = rc;
    }
}
