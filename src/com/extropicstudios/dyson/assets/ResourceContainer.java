package com.extropicstudios.dyson.assets;

import java.util.Collection;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Font;
import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.Sound;
import org.newdawn.slick.tiled.TiledMap;

public interface ResourceContainer {

	@Deprecated
	Font getDefaultFont();
	
	Collection<String> fontList();
	Collection<String> animationList();
	Collection<String> soundList();
	Collection<String> musicList();
	Collection<String> mapList();
	Collection<String> imageList();
	
	Font getFont(String font);
	Animation getAnimation(String animation);
	Sound getSound(String sound);
	Music getMusic(String music);
	TiledMap getMap(String map);
	Image getImage(String image);
}
