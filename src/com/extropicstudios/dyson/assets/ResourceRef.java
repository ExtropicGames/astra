package com.extropicstudios.dyson.assets;

public class ResourceRef {

	public enum Type {
		FONT,
		IMAGE,
		MAP,
		MUSIC,
		SOUND,
		ANIMATION
	}
	
	public String path;
	public String identifier;
	public Type type;
	
	// for fonts
	public int r;
	public int g;
	public int b;
	
	// for sprite sheets
	public int tw;
	public int th;
	public int frameDuration;
	public int padding;
	
	public ResourceRef(String path, String identifier, Type type) {
		this.path = path;
		this.identifier = identifier;
		this.type = type;
	}
	
	public ResourceRef r(int r) {
		this.r = r;
		return this;
	}
	
	public ResourceRef g(int g) {
		this.g = g;
		return this;
	}
	
	public ResourceRef b(int b) {
		this.b = b;
		return this;
	}
	
	public ResourceRef tw(int tw) {
		this.tw = tw;
		return this;
	}
	
	public ResourceRef th(int th) {
		this.th = th;
		return this;
	}
	
	public ResourceRef frameDuration(int duration) {
		this.frameDuration = duration;
		return this;
	}
	
	public ResourceRef padding(int padding) {
		this.padding = padding;
		return this;
	}
}
