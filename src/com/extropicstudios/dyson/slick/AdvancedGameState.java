package com.extropicstudios.dyson.slick;

import org.newdawn.slick.state.BasicGameState;

import com.extropicstudios.dyson.assets.ResourceContainer;

public abstract class AdvancedGameState extends BasicGameState {

    private final int stateID;
    
    protected final ResourceContainer rc;
    
    @Override
    public int getID() {
        return stateID;
    }

    public AdvancedGameState(int stateID, ResourceContainer rc) {
        this.stateID = stateID;
        this.rc = rc;
    }
}
