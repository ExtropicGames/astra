package com.extropicstudios.dyson.slick;

import java.awt.Color;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Font;
import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.font.effects.Effect;
import org.newdawn.slick.tiled.TiledMap;

import com.extropicstudios.dyson.assets.ResourceContainer;
import com.extropicstudios.dyson.assets.ResourceRef;
import com.extropicstudios.dyson.assets.ResourceRef.Type;

public class SlickResourceContainer implements ResourceContainer {

	private String defaultFont = null;
	
	private Map<String, Font> fontMap = new HashMap<String, Font>();
	private Map<String, Animation> animationMap = new HashMap<String, Animation>();
	private Map<String, Image> imageMap = new HashMap<String, Image>();
	private Map<String, Sound> soundMap = new HashMap<String, Sound>();
	private Map<String, TiledMap> mapMap = new HashMap<String, TiledMap>();
	private Map<String, Music> musicMap = new HashMap<String, Music>();
	
	@Override
	public Collection<String> animationList() {
		return animationMap.keySet();
	}
	@Override
	public Collection<String> fontList() {
		return fontMap.keySet();
	}
	@Override
	public Collection<String> imageList() {
		// TODO Auto-generated method stub
		return imageMap.keySet();
	}
	@Override
	public Collection<String> mapList() {
		// TODO Auto-generated method stub
		return mapMap.keySet();
	}
	@Override
	public Collection<String> musicList() {
		// TODO Auto-generated method stub
		return musicMap.keySet();
	}
	@Override
	public Collection<String> soundList() {
		// TODO Auto-generated method stub
		return soundMap.keySet();
	}
	
	@Override
	public Font getFont(String identifier) {
		return fontMap.get(identifier);
	}
	@Override
	public Animation getAnimation(String identifier) {
		return animationMap.get(identifier);
	}
	@Override
	public Image getImage(String identifier) {
		return imageMap.get(identifier);
	}
	@Override
	public Sound getSound(String identifier) {
		return soundMap.get(identifier);
	}
	@Override
	public TiledMap getMap(String identifier) {
		return mapMap.get(identifier);
	}
	@Override
	public Music getMusic(String identifier) {
		return musicMap.get(identifier);
	}
	
	public void loadMusic(String file, String identifier) throws SlickException {
		if (file == null || file.length() == 0) {
			throw new SlickException("Music file [" + identifier + "] has an invalid path.");
		}
		
		Music music = null;
		try {
			music = new Music(file);
		} catch (SlickException e) {
			throw new SlickException("Music file " + identifier + " could not be loaded from " + file + ": ", e);
		}
		
		this.musicMap.put(identifier, music);
	}
	
	public void loadAnimation(String file,  String identifier, int tileWidth, int tileHeight, int frameDuration, int padding) throws SlickException {
		if (file == null || file.length() == 0) {
			throw new SlickException("Animation Resource [" + identifier + "] has an invalid path.");
		}
		
		SpriteSheet sheet = null;
		try {
			sheet = new SpriteSheet(file, tileWidth, tileHeight, padding);
		} catch (SlickException e) {
			throw new SlickException("Animation " + identifier + " could not be loaded from " + file + ": ", e);
		}
		
		Animation anim = new Animation(sheet, frameDuration);
		
		this.animationMap.put(identifier, anim);
	}
	
	public void loadSound(String file, String identifier) throws SlickException {
		if (file == null || file.length() == 0)
			throw new SlickException("Sound resource [" + identifier + "] has invalid path");
 
		Sound sound = null;
 
		try {
			sound = new Sound(file);
		} catch (SlickException e) {
			throw new SlickException("Sound " + identifier + " could not be loaded from " + file + ": ", e);
		}
 
		this.soundMap.put(identifier, sound);
	}
	
	public void loadMap(String file, String identifier) throws SlickException {
		if (file == null || file.length() == 0)
			throw new SlickException("Tiled Map resource [" + identifier + "] has invalid path");
 
		TiledMap tmap = null;
 
		try {
			tmap = new TiledMap(file);
		} catch (SlickException e) {
			throw new SlickException("Map " + identifier + " could not be loaded from " + file + ": ", e);
		}
 
		mapMap.put(identifier, tmap);
	}
	
	public void loadImage(String file, String identifier) throws SlickException {
		if (file == null || file.length() == 0) {
			throw new SlickException("Image [" + identifier + "] has an invalid path.");
		}
		
		Image image = null;
		try {
			image = new Image(file);
		} catch (SlickException e) {
			throw new SlickException("Image " + identifier + " could not be loaded from " + file + ": ", e);
		}
		
		this.imageMap.put(identifier, image);
	}
	
	public void loadFont(String file, String identifier, int r, int g, int b) throws SlickException {
		if (file == null || file.length() == 0) {
			throw new SlickException("Font [" + identifier + "] has an invalid path.");
		}
		
		UnicodeFont uniFont = null;
		
		try {
			uniFont = new UnicodeFont(file, 10, false, false);
		} catch (SlickException e) {
			throw new SlickException("Font " + identifier + " could not be loaded from " + file + ": ", e); 
		}
		
		uniFont.addAsciiGlyphs();

		@SuppressWarnings("unchecked")
		List<Effect> effectsList = uniFont.getEffects();
		
		effectsList.add(new ColorEffect(new Color(r, g, b)));
		
		try {
			uniFont.loadGlyphs();
		} catch (SlickException e) {
			throw new SlickException("Font " + identifier + " did not contain ASCII glyphs.");
		}
 
		this.fontMap.put(identifier, uniFont);
	}

	public void loadResource(ResourceRef resource) throws SlickException {
		System.out.println("Loading resource " + resource.path);
		if (resource.type == Type.FONT) {
			loadFont(resource.path, resource.identifier, resource.r, resource.g, resource.b);
		} else if (resource.type == Type.ANIMATION) {
			loadAnimation(resource.path, resource.identifier, resource.tw, resource.th, resource.frameDuration, resource.padding);
		} else if (resource.type == Type.IMAGE) {
			loadImage(resource.path, resource.identifier);
		} else if (resource.type == Type.MAP) {
			loadMap(resource.path, resource.identifier);
		} else if (resource.type == Type.MUSIC) {
			loadMusic(resource.path, resource.identifier);
		} else if (resource.type == Type.SOUND) {
			loadSound(resource.path, resource.identifier);
		} else {
			throw new SlickException("Unknown resource type " + resource.type);
		}
	}

	public void setDefaultFont(String font) {
		this.defaultFont = font;
	}
	
	@Override
	public Font getDefaultFont() {
		return fontMap.get(defaultFont);
	}
}
