package com.extropicstudios.astra;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.extropicstudios.astra.states.LoadingState;
import com.extropicstudios.astra.states.MainMenuState;
import com.extropicstudios.dyson.assets.ResourceContainer;
import com.extropicstudios.dyson.slick.SlickResourceContainer;

public class AstraGame extends StateBasedGame {

	public static final int MAIN_MENU_STATE = 1;
	public static final int LOADING_STATE = 2;
	
	public AstraGame() {
		super("Astra");
		ResourceContainer rc = new SlickResourceContainer();
		// TODO: for some reason, whichever is loaded first is the one entered, instead
		// of obeying enterState like it should.
		this.addState(new LoadingState(LOADING_STATE, rc));
		this.addState(new MainMenuState(MAIN_MENU_STATE, rc));
		this.enterState(LOADING_STATE);
	}
	
	public static void main(String[] args) throws SlickException {
		AppGameContainer app = new AppGameContainer(new AstraGame());
		
		app.setShowFPS(false);
		app.setDisplayMode(800, 600, false);
		app.start();
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.getState(MAIN_MENU_STATE).init(gc, this);
		this.getState(LOADING_STATE).init(gc, this);
	}

}
