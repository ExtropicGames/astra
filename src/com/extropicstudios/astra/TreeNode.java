package com.extropicstudios.astra;

import java.util.LinkedList;
import java.util.List;

public class TreeNode<T> {
	
	private T data;
	private TreeNode<T> parent;
	private List<TreeNode<T>> children;
	
	public void addChild(TreeNode<T> child) {
		if (children == null)
			children = new LinkedList<TreeNode<T>>();
		children.add(child);
		child.setParent(this);
	}
	
	public void setData(T data) {
		this.data = data;
	}
	
	public T getData() {
		return data;
	}
	
	public TreeNode<T> getParent() {
		return parent;
	}
	
	public void setParent(TreeNode<T> parent) {
		this.parent = parent; 
	}
}
