package com.extropicstudios.astra;

import java.io.InputStream;
import java.util.Scanner;

/**
 * A parser for S-Expressions, as used in Lisp.
 * @author josh
 */
public class SExpressionReader {

	// TODO: This code doesn't really work right yet, but it's sort of in the right direction.
	
	public boolean read(InputStream is) {
		Scanner s = new Scanner(is);
		while (s.hasNextLine()) {
			String line = s.nextLine();
			StringBuilder token = new StringBuilder();
			
			Tree<String> syntaxTree = new Tree<String>();
			
			TreeNode<String> pointer = syntaxTree.getRoot();
			
			for (char c : line.toCharArray()) {
				if (c == '(') {
					TreeNode<String> node = new TreeNode<String>();
					pointer.addChild(node);
					pointer = node;
					// add paren to stack
				} else if (c == ')') {
					pointer = pointer.getParent();
					// pop paren from stack
				} else if (Character.isWhitespace(c)) {
					// put token on token stack
					pointer.setData(token.toString());
					token = new StringBuilder();
				} else {
					token.append(c);
				} 
			}
		}
		
		return true;
	}
}
