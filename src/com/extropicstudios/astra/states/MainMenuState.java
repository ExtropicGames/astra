package com.extropicstudios.astra.states;

import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import com.extropicstudios.astra.models.ConnectionType;
import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.astra.models.systems.Cryogenics;
import com.extropicstudios.astra.models.systems.GeneticsLab;
import com.extropicstudios.astra.models.systems.Gymnasium;
import com.extropicstudios.astra.models.systems.Hydroponics;
import com.extropicstudios.astra.models.systems.Library;
import com.extropicstudios.astra.models.systems.LifeSupport;
import com.extropicstudios.astra.models.systems.Maintenance;
import com.extropicstudios.astra.models.systems.Navigation;
import com.extropicstudios.astra.models.systems.Person;
import com.extropicstudios.astra.models.systems.Power;
import com.extropicstudios.astra.models.systems.Propulsion;
import com.extropicstudios.astra.models.systems.SensorArray;
import com.extropicstudios.astra.models.systems.ShipAI;
import com.extropicstudios.astra.models.systems.WasteReprocessing;
import com.extropicstudios.dyson.assets.ResourceContainer;
import com.extropicstudios.dyson.gui.WindowFrame;
import com.extropicstudios.dyson.slick.AdvancedGameState;

public class MainMenuState extends AdvancedGameState implements MouseListener {
	
	private SystemNode dragNode = null;
	private Vector2f dragPoint = new Vector2f(0,0);
	private SystemNode selectedNode = null;
	
	private List<SystemNode> nodes;
	
	private WindowFrame window;

	public MainMenuState(int stateID, ResourceContainer rc) {
	    super(stateID, rc);
	}
	
	@Override
	public void enter(GameContainer gc, StateBasedGame sbg) {
		Log.debug("Entering MainMenuState");
	}
	
	@Override
	public void leave(GameContainer gc, StateBasedGame sbg) {
		Log.debug("Exiting MainMenuState");
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		nodes = new LinkedList<SystemNode>();
		//nodes.add(new Cryogenics());
		//nodes.add(new GeneticsLab());
		//nodes.add(new Gymnasium());
		SystemNode hydroponics = new Hydroponics(rc);
		nodes.add(hydroponics);
		//nodes.add(new Library());
		//nodes.add(new LifeSupport());
		//nodes.add(new Maintenance());
		//nodes.add(new Navigation());
		SystemNode power = new Power(rc);
		nodes.add(power);
		//nodes.add(new Propulsion());
		//nodes.add(new SensorArray());
		//nodes.add(new ShipAI());
		SystemNode wasteReprocessing = new WasteReprocessing(rc);
		nodes.add(wasteReprocessing);
		//nodes.add(new Person());
		
		SystemNode.link(power, hydroponics, ConnectionType.ELECTRICITY);
		SystemNode.link(hydroponics, wasteReprocessing, ConnectionType.BIOLOGICAL_WASTE);
		
		//for (SystemNode n : nodes) {
		//	System.out.println(n.toString());
		//}
		window = new WindowFrame(rc, 600, 0, 200, 600);
		window.setBorder(15);
		window.setRounded(true);
		window.setWindowColors(new Color(33,33,33), new Color(99,99,99));
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		for (SystemNode n : nodes) {
			if (selectedNode == n)
				n.render(g, rc, new Color(255,0,0));
			else
				n.render(g, rc, new Color(255,255,255));
		}
		window.render(g, 0, 0);
		if (selectedNode != null) {
			window.drawString(0, 0, selectedNode.getName(), rc.getDefaultFont());
		}
	}

	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {
		for (SystemNode n : nodes) {
			if (n.getLocation().contains(x, y)) {
				selectedNode = n;
				break;
			}
		}
	}
	
	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		if (dragNode == null) {
			for (SystemNode n : nodes) {
				if (n.getLocation().contains(oldx, oldy)) {
					dragPoint.x = oldx - n.getLocation().getX();
					dragPoint.y = oldy - n.getLocation().getY();
					dragNode = n;
					break;
				}
			}
		}
		
		if (dragNode != null) {
			dragNode.moveTo(newx - dragPoint.x, newy - dragPoint.y);
		}
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		Input i = gc.getInput();
		if (!i.isMouseButtonDown(0)) {
			dragNode = null;
		}
	}

}
