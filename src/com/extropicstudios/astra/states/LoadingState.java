package com.extropicstudios.astra.states;

import java.util.Stack;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import com.extropicstudios.astra.AstraGame;
import com.extropicstudios.dyson.assets.ResourceContainer;
import com.extropicstudios.dyson.assets.ResourceRef;
import com.extropicstudios.dyson.slick.AdvancedGameState;
import com.extropicstudios.dyson.slick.SlickResourceContainer;

/**
 * In this state, all the resources are loaded.
 * @author josh
 */
public class LoadingState extends AdvancedGameState {

    private boolean fontLoaded = false;
	
	private Stack<ResourceRef> resourceStack = new Stack<ResourceRef>();
	
	public LoadingState(int stateID, ResourceContainer rc) {
        super(stateID, rc);
    }
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		
		//resourceStack.push(new ResourceRef("data/fonts/prstart.ttf", "prstart", ResourceRef.Type.FONT));
	}
	
	@Override
	public void enter(GameContainer gc, StateBasedGame sbg) {
		Log.debug("Entering LoadingState");
	}
	
	@Override
	public void leave(GameContainer gc, StateBasedGame sbg) {
		Log.debug("Exiting LoadingState");
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		if (fontLoaded && !resourceStack.isEmpty()) {
			String filename = resourceStack.peek().path;
			rc.getFont("prstart").drawString(0, 0, filename);
		}
		// TODO Auto-generated method stub
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		if (!fontLoaded) {
			((SlickResourceContainer) rc).loadFont("data/fonts/prstart.ttf", "prstart", 0, 0, 0);
			fontLoaded = true;
		}
		if (resourceStack.isEmpty()) {
			sbg.enterState(AstraGame.MAIN_MENU_STATE);
		} else {
//            try {
//                rc.loadResource(resourceStack.pop());
//            } catch (IOException e) {
//                throw new SlickException("Could not load resource!", e);
//            }
		}
	}
}
