package com.extropicstudios.astra.models.systems;

import org.newdawn.slick.geom.Rectangle;

import com.extropicstudios.astra.models.ConnectionType;
import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class Cryogenics extends SystemNode {

	public Cryogenics(ResourceContainer rc) {
	    super(rc);
		this.name = "Cryogenics";
		this.location = new Rectangle(50, 50, 100, 100);
		this.addInput(ConnectionType.ELECTRICITY);
	}
}
