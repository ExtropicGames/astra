package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class Navigation extends SystemNode {

	public Navigation(ResourceContainer rc) {
	    super(rc);
		this.name = "Navigation";
	}
}
