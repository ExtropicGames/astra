package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class Library extends SystemNode {

	public Library(ResourceContainer rc) {
	    super(rc);
		this.name = "Library";
	}
}
