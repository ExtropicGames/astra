package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.ConnectionType;
import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class Power extends SystemNode {

	public Power(ResourceContainer rc) {
	    super(rc);
		this.name = "Power";
		this.addOutput(ConnectionType.ELECTRICITY);
	}
}
