package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class GeneticsLab extends SystemNode {

	public GeneticsLab(ResourceContainer rc) {
	    super(rc);
		this.name = "Genetics Lab";
	}
}
