package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class Gymnasium extends SystemNode {

	public Gymnasium(ResourceContainer rc) {
	    super(rc);
		this.name = "Gymnasium";
	}
}
