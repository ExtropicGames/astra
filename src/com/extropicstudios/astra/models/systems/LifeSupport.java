package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.ConnectionType;
import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class LifeSupport extends SystemNode {

	
	public LifeSupport(ResourceContainer rc) {
	    super(rc);
		this.name = "Life Support";
		this.addInput(ConnectionType.ELECTRICITY);
	}
}
