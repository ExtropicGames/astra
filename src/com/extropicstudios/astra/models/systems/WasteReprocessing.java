package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.ConnectionType;
import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class WasteReprocessing extends SystemNode {
	
	public WasteReprocessing(ResourceContainer rc) {
	    super(rc);
		this.name = "Waste Reprocessing";
		this.addInput(ConnectionType.BIOLOGICAL_WASTE);
		this.addInput(ConnectionType.FERROUS_WASTE);
		this.addInput(ConnectionType.ELECTRICITY);
		
		this.addOutput(ConnectionType.SUBSTRATE);
		this.addOutput(ConnectionType.WATER);
		this.addOutput(ConnectionType.FERROUS_MATERIAL);
	}
}
