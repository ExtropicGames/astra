package com.extropicstudios.astra.models.systems;

import org.newdawn.slick.geom.Circle;

import com.extropicstudios.astra.models.ConnectionType;
import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class Person extends SystemNode {

	public Person(ResourceContainer rc) {
	    super(rc);
		this.name = "Person";
		this.location = new Circle(0, 0, 5);
		this.addInput(ConnectionType.FOOD);
		this.addInput(ConnectionType.WATER);
		this.addInput(ConnectionType.AIR);
		
		this.addOutput(ConnectionType.BIOLOGICAL_WASTE);
	}
}
