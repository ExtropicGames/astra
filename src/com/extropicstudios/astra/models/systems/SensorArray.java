package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class SensorArray extends SystemNode {

	public SensorArray(ResourceContainer rc) {
	    super(rc);
		this.name = "Sensor Array";
	}
}
