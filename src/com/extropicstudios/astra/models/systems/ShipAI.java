package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class ShipAI extends SystemNode {

	public ShipAI(ResourceContainer rc) {
	    super(rc);
		this.name = "Ship AI";
	}
}
