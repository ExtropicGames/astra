package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.ConnectionType;
import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class Propulsion extends SystemNode {

	public Propulsion(ResourceContainer rc) {
	    super(rc);
		this.name = "Propulsion";
		this.addInput(ConnectionType.ELECTRICITY);
	}
}
