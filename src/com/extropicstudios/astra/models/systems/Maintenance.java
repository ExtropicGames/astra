package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class Maintenance extends SystemNode {
	
	public Maintenance(ResourceContainer rc) {
	    super(rc);
		this.name = "Maintenance";
	}
}
