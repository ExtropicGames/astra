package com.extropicstudios.astra.models.systems;

import com.extropicstudios.astra.models.ConnectionType;
import com.extropicstudios.astra.models.SystemNode;
import com.extropicstudios.dyson.assets.ResourceContainer;

public class Hydroponics extends SystemNode {
	public Hydroponics(ResourceContainer rc) {
	    super(rc);
		this.name = "Hydroponics";
		this.addInput(ConnectionType.ELECTRICITY);
		this.addInput(ConnectionType.SUBSTRATE);
		this.addInput(ConnectionType.WATER);
		this.addInput(ConnectionType.SEEDS);

		this.addOutput(ConnectionType.FOOD);
		this.addOutput(ConnectionType.BIOLOGICAL_WASTE);
		this.addOutput(ConnectionType.SEEDS);

		// by default we link the seeds out to the seeds in, creating a loop
		SystemNode.link(this, this, ConnectionType.SEEDS);
	}
}
