package com.extropicstudios.astra.models;

public enum ConnectionType {
	WATER,
	SUBSTRATE,
	ELECTRICITY,
	SEEDS,
	FOOD,
	BIOLOGICAL_WASTE,
	FERROUS_WASTE,
	FERROUS_MATERIAL,
	AIR
}
