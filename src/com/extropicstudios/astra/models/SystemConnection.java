package com.extropicstudios.astra.models;

import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class SystemConnection {

	private double throughput;
	private List<SystemNode> connections = new LinkedList<SystemNode>();
	
	public boolean link(SystemNode node) {
		return connections.add(node);
	}
	
	public boolean unlink(SystemNode node) {
		return connections.remove(node);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (connections.isEmpty())
			sb.append("nothing ");
		for (SystemNode n : connections) {
			sb.append(n.getName());
			sb.append(", ");
		}
		sb.append("at ");
		sb.append(throughput);
		sb.append(" units/tick");
		return sb.toString();
	}

	public void render(Graphics g, SystemNode origin) {
		this.render(g, origin, new Color(255,255,255));
	}
	
	public void render(Graphics g, SystemNode origin, Color color) {
		g.setColor(color);
		float x1 = origin.location.getCenterX();
		float y1 = origin.location.getCenterY();
		for (SystemNode target : connections) {
			float x2 = target.location.getCenterX();
			float y2 = target.location.getCenterY();
			g.drawLine(x1, y1, x2, y2);
		}
	}
}
