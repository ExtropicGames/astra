package com.extropicstudios.astra.models;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import com.extropicstudios.dyson.assets.ResourceAccessor;
import com.extropicstudios.dyson.assets.ResourceContainer;

public abstract class SystemNode extends ResourceAccessor {

	public SystemNode(ResourceContainer rc) {
        super(rc);
    }

    protected Shape location = new Rectangle(0,0,100,100);
	protected String name = "null";

	private Map<ConnectionType, SystemConnection> inputs = new HashMap<ConnectionType, SystemConnection>();
	private Map<ConnectionType, SystemConnection> outputs = new HashMap<ConnectionType, SystemConnection>();

	public Collection<SystemConnection> getInputs() { return inputs.values(); }
	public Collection<SystemConnection> getOutputs() { return outputs.values(); }

	public String getName() {
		return name;
	}

	public void addInput(ConnectionType type) {
		inputs.put(type, new SystemConnection());
	}

	public void addOutput(ConnectionType type) {
		outputs.put(type, new SystemConnection());
	}

	public static boolean link(SystemNode sendNode, SystemNode receiveNode, ConnectionType type) {
		SystemConnection oc = sendNode.outputs.get(type);
		SystemConnection ic = receiveNode.inputs.get(type);
		if (oc == null || ic == null) {
			System.out.println("Could not link mismatched nodes.");
			return false;
		}

		return oc.link(receiveNode) && ic.link(sendNode);
	}

	public static boolean unlink(SystemNode sendNode, SystemNode receiveNode, ConnectionType type) {
		SystemConnection oc = sendNode.outputs.get(type);
		SystemConnection ic = receiveNode.inputs.get(type);
		if (oc == null || ic == null) {
			System.out.println("Could not unlink mismatched nodes.");
			return false;
		}

		return oc.unlink(receiveNode) && ic.unlink(sendNode);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(this.getName());
		sb.append(" has: ");

		if (inputs.isEmpty())
			sb.append("no inputs ");
		for (Entry<ConnectionType, SystemConnection> e : inputs.entrySet()) {
			sb.append("Input ");
			sb.append(e.getKey().toString());
			sb.append(" connected to ");
			sb.append(e.getValue().toString());
			sb.append(", ");
		}
		sb.append("and ");

		if (outputs.isEmpty())
			sb.append("no outputs ");
		for (Entry<ConnectionType, SystemConnection> e : outputs.entrySet()) {
			sb.append("Output ");
			sb.append(e.getKey().toString());
			sb.append(" connected to ");
			sb.append(e.getValue().toString());
			sb.append(", ");
		}

		return sb.toString();
	}
	public void render(Graphics g, ResourceContainer rc) {
		this.render(g, rc, new Color(255,255,255));
	}
	public Shape getLocation() {
		return location;
	}
	public void moveTo(float x, float y) {
		location.setLocation(x, y);
	}

	public void render(Graphics g, ResourceContainer rc, Color color) {
		g.setColor(color);
		g.fill(location);
		Font font = rc.getFont("prstart");
		font.drawString(location.getX(), location.getCenterY(), name);
		for (SystemConnection sc : inputs.values()) {
			sc.render(g, this);
		}
	}
}
