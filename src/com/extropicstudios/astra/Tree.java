package com.extropicstudios.astra;

public class Tree<T> {
	
	private TreeNode<T> root = new TreeNode<T>();
	
	public TreeNode<T> getRoot() {
		return root;
	}

}
