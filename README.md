
# Abstract

Concept: Oregon Trail in Space.

A top-down simulation game similar to Dwarf Fortress. The goal of the game is to successfully fly a generation ship to a distant star and 
establish a colony there. At the beginning of the game, you design your generation ship, attempting to balance population, resources, and 
limited supplies. The 1.0 release of the game will only involve guiding your generation ship to the destination star and trying to keep as 
many colonists alive as possible until you reach your destination. The success of the future colony will depend on how many colonists make 
it and how much of your resources you were able to preserve.

Future improvements could include actually playing as the established colony, or playing as a government on Earth trying to build a generation 
ship. Far-future improvements could include colonies eventually becoming advanced enough to construct their own generation ships and establish 
further colonies.

## Compilation

### Ubuntu 23.10

- `sudo apt install liblwjgl-java`